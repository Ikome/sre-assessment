resource "aws_route53_zone" "podinfo_zone" {
  name = "domain-name.com."  # Update with your hosted zone name, note the trailing dot
}

resource "aws_route53_record" "ingress-domain1" {
  zone_id = aws_route53_zone.podinfo_zone.zone_id
  name    = "podinfo-dev.domain-name.com"
  type    = "CNAME"
  ttl     = "300"  # TTL in seconds
  records = [
    "your-load-balancer-dns-name.amazonaws.com"  # Replace with your load balancer DNS name
  ]
}

resource "aws_route53_record" "ingress-domain2" {
  zone_id = aws_route53_zone.example_zone.zone_id
  name    = "podinfo.domain-name.com"
  type    = "CNAME"
  ttl     = "300"  # TTL in seconds
  records = [
    "your-load-balancer-dns-name.amazonaws.com"  # Replace with your load balancer DNS name
  ]
}