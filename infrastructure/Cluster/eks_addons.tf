module "aws_load_balancer_controller" {
  source      = "aws-ia/eks-blueprints-addon/aws"
  version     = "~> 1.1.1"
  name        = "aws-load-balancer-controller"
  description = "A Helm chart to deploy aws-load-balancer-controller"
  namespace   = "kube-system"
  # namespace creation is false here as kube-system already exists by default
  create_namespace = false
  chart            = "aws-load-balancer-controller"
  chart_version    = "1.7.1"
  repository       = "https://aws.github.io/eks-charts"

  wait          = true
  wait_for_jobs = true

  set = [
    {
      name  = "serviceAccount.name"
      value = "aws-load-balancer-controller-sa"
    },
    {
      name  = "clusterName"
      value = module.eks.cluster_name
    }
  ]

  # IAM role for service account (IRSA)
  create_role          = true
  set_irsa_names       = ["serviceAccount.annotations.eks\\.amazonaws\\.com/role-arn"]
  role_name            = "aws-lb-controller"
  role_name_use_prefix = true
  role_path            = "/"
  role_description     = "IRSA for aws-load-balancer-controller project"

  source_policy_documents = data.aws_iam_policy_document.aws_load_balancer_controller[*].json
  policy_statements       = []
  policy_name_use_prefix  = true
  policy_description      = "IAM Policy for AWS Load Balancer Controller"

  oidc_providers = {
    this = {
      provider_arn = module.eks.oidc_provider_arn
      # namespace is inherited from chart
      service_account = "aws-load-balancer-controller-sa"
    }
  }
}
