region              = "us-east-1"
vpc_name            = "sre-devops-vpc"
vpc_cidr            = "10.0.0.0/16"
vpc_private_subnets = ["10.0.1.0/24", "10.0.2.0/24", "10.0.3.0/24"]
vpc_public_subnets  = ["10.0.4.0/24", "10.0.5.0/24", "10.0.6.0/24"]
cluster_name        = "sre-devops-cluster"
eks_auth_map_users = [
  {
    userarn  = "arn:aws:iam::080266302756:user/admin"
    username = "user1"
    groups   = ["system:masters"]
  },
  {
    userarn  = "arn:aws:iam::080266302756:user/Ikome"
    username = "user2"
    groups   = ["system:masters"]
  }
]
