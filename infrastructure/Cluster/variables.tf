variable "vpc_name" {
  type    = string
  default = ""
}

variable "vpc_cidr" {
  type    = string
  default = ""
}

variable "vpc_private_subnets" {
  type    = list(string)
  default = [""]
}

variable "vpc_public_subnets" {
  type    = list(string)
  default = [""]
}


variable "cluster_name" {
  type    = string
  default = ""
}

variable "eks_auth_map_users" {
  type = list(object({
    userarn  = string
    username = string
    groups   = list(string)
  }))

  default = [{
    userarn  = ""
    username = ""
    groups   = [""]
  }]
}

variable "eks_auth_map_roles" {
  type = list(object({
    rolearn  = string
    username = string
    groups   = list(string)
  }))

  default = [{
    rolearn  = ""
    username = ""
    groups   = [""]
  }]
}

variable "hosted_zone_id" {
  type    = string
  default = ""
}

variable "region" {
  description = "AWS region"
  type        = string
  default     = "us-east-1"
}